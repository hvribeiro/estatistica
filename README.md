# Estatística

*Notas de aula do curso de Mecânica Estatística - DFI/UEM*

Vamos usar o livro: [**Introdução à Física Estatística. Salinas, Silvio.**](http://library.lol/main/EB2254C5CD584943BF19E51F2E06B715) 

Lista de problemas disponível [aqui](http://complex.pfi.uem.br/ribeiro/listas/estatistica.pdf).

- [Aula 01 - 11/01/2021](https://gitlab.com/hvribeiro/estatistica/-/blob/master/aulas/aula01.png)
- [Aula 02 - 13/01/2021](https://gitlab.com/hvribeiro/estatistica/-/blob/master/aulas/aula02.png)
- [Aula 03 - 18/01/2021](https://gitlab.com/hvribeiro/estatistica/-/blob/master/aulas/aula03.png)
- [Aula 04 - 21/01/2021](https://gitlab.com/hvribeiro/estatistica/-/blob/master/aulas/aula04.png)
- [Aula 05 - 25/01/2021](https://gitlab.com/hvribeiro/estatistica/-/blob/master/aulas/aula05.png)
- [Aula 06 - 27/01/2021](https://gitlab.com/hvribeiro/estatistica/-/blob/master/aulas/aula06.png)